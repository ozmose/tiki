<?php

namespace Tiki\Lib\Iot;

enum DrawflowNodeType
{
    case Template;
    case User_input;
    case User_choice;
}
